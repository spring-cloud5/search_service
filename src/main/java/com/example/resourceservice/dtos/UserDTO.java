package com.example.resourceservice.dtos;

public class UserDTO {
    private String id;
    private String name;
    private String age;
    private String email;
    private String password;
    private String Error;
    private String image;
    private String image_type;

    public String getImage_type() {
        return image_type;
    }

    public void setImage_type(String image_type) {
        this.image_type = image_type;
    }

    public void setError(String error) {
        Error = error;
    }

    public String getError() {
        return Error;
    }

    public UserDTO() {
        this.Error = " ";
    }

    public UserDTO(String error) {
        this.Error = error;
    }


    public UserDTO(String id, String name, String age, String email, String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
    }

    public UserDTO(String id, String name, String age, String email, String password, String image, String image_type) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        this.image = image;
        this.image_type = image_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
