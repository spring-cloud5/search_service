package com.example.resourceservice.dtos;

public class PostResponse {

    private String  note;
    private String title;
    private String  url;
    private String date;
    private UserDTO userDTO;

    public PostResponse(String note, String title, String url, String date, UserDTO userDTO) {
        this.note = note;
        this.title = title;
        this.url = url;
        this.date = date;
        this.userDTO = userDTO;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getNote() {
        return note;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getDate() {
        return date;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }
}
