package com.example.resourceservice.feignService;

import com.example.resourceservice.models.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "userservice", fallback = UserService.UserServiceFallback.class)
public interface UserService {

    @GetMapping(value = "/getById/{id}")
    User getUserById(@PathVariable Long id);

    @Component
    static class UserServiceFallback implements UserService {

        @GetMapping(value = "/getById/{id}")
        public User getUserById(@PathVariable Long id) {
            return null;
        }
    }

}
