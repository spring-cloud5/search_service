package com.example.resourceservice.feignService;

import com.example.resourceservice.models.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "bookservice",fallback = BookService.BookServiceFallBack.class)
public interface BookService {

    @PostMapping( value = "/searchByKeywordWithoutUsers")
    List<Post> searchByKerword(@RequestParam() String keyword);

    @Component
    static class BookServiceFallBack implements BookService{
        @Override
        public List<Post> searchByKerword(String keyword) {
            return null;
        }
    }
}
