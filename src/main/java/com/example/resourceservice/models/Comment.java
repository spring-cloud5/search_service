package com.example.resourceservice.models;


import java.util.Date;


public class Comment {
    private Long id;

    private String note;

    private Date date;

    private Long user_id;


    public Comment(String note, Long user_id) {
        this.note = note;
        this.user_id = user_id;
    }
    public Comment(){}

    public void setId(Long id) {
        this.id = id;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public void setDate(Date date) {
        this.date = date;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getId() {
        return id;
    }

    public String getNote() {
        return note;
    }


    public Date getDate() {
        return date;
    }

    public Long getUser_id() {
        return user_id;
    }
}