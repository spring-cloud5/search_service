package com.example.resourceservice.models;


public class User {

    private Long id;

    private String age;

    private String name;

    private String email;

    private String password;

    private String image;

    private String image_type;

    public User(String age, String name, String email, String password, String image, String image_type) {
        this.age = age;
        this.name = name;
        this.email = email;
        this.password = password;
        this.image = image;
        this.image_type = image_type;
    }

    public User(String email) {
        this.email = email;
    }

    public User(){

    }

    public void setImage_type(String image_type) {
        this.image_type = image_type;
    }

    public String getImage_type() {
        return image_type;
    }

    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public String getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
