package com.example.resourceservice.models;


import java.util.Date;
import java.util.Set;


public class Post {
    private Long id;

    private String title;

    private String note;

    private Date date;

    private Long user_id;

    private Set<Comment> comments;

    private User user;

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post(String title, String note, Date date, Long user_id) {
        this.title = title;
        this.note = note;
        this.date = date;
        this.user_id = user_id;
    }

    public Post() {

    }


    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long
    getUser_id() {
        return user_id;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getNote() {
        return note;
    }

    public Date getDate() {
        return date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}