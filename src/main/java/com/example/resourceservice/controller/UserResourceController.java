package com.example.resourceservice.controller;

import com.example.resourceservice.feignService.*;
import com.example.resourceservice.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class UserResourceController {

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;
    
    @PostMapping( value = "/searchByKeyword")
    @CrossOrigin(origins = "*")
    public List<Post> searchByKerword (@RequestParam() String keyword){
        List<Post> posts = bookService.searchByKerword(keyword);
        for(Post post : posts){
            post.setUser(userService.getUserById(post.getUser_id()));
        }
        return posts;
    }
}
